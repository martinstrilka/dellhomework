package cz.martinstrilka.dell.view.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import cz.martinstrilka.dell.controller.Controller;
import cz.martinstrilka.dell.view.ToolBar;

public class ToolBarActionListener implements ActionListener {

	private Controller controller;
	
	public ToolBarActionListener(Controller controller) {
		this.controller = controller;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();

		switch(command) {
		case ToolBar.RECTANGLE_CMD:
			controller.switchStateRectangle();
			break;
		case ToolBar.TRIANGLE_CMD:
			controller.switchStateTriangle();
			break;
		case ToolBar.CIRCLE_CMD:
			controller.switchStateCircle();
			break;
		case ToolBar.SELECT_CMD:
			controller.switchStateSelect();
			break;
		default:
			controller.groupSelected();
		}
	}

}
