package cz.martinstrilka.dell.view.listeners;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import cz.martinstrilka.dell.controller.Controller;

public class WindowKeyListener implements KeyListener {

	private Controller controller;
	
	public WindowKeyListener(Controller controller) {
		this.controller = controller;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getKeyChar() == KeyEvent.VK_DELETE) 
			controller.deleteSelected();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		//System.out.println("Key pressed: " + e.getKeyCode());
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

}
