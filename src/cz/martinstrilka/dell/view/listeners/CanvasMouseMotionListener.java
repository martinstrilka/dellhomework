package cz.martinstrilka.dell.view.listeners;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import cz.martinstrilka.dell.controller.Controller;

public class CanvasMouseMotionListener extends MouseAdapter {

private Controller controller;
	
	public CanvasMouseMotionListener(Controller controller) {
		this.controller = controller;
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		controller.mouseDragged(e.getX(), e.getY());
	}
	
	@Override
		public void mouseMoved(MouseEvent e) {
			controller.mouseMoved(e.getX(), e.getY());
		}
}
