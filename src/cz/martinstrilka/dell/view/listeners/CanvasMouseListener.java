package cz.martinstrilka.dell.view.listeners;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import cz.martinstrilka.dell.controller.Controller;

public class CanvasMouseListener extends MouseAdapter {

	private Controller controller;
	
	public CanvasMouseListener(Controller controller) {
		this.controller = controller;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		controller.mousePressed(e.getX(), e.getY());
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		controller.mouseReleased(e.getX(), e.getY());
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		controller.mouseClicked(e.getX(), e.getY());
	}

}
