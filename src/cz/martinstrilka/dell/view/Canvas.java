package cz.martinstrilka.dell.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import cz.martinstrilka.dell.shapes.Model;

public class Canvas extends JPanel {
	
	private static final long serialVersionUID = -8536040424764141119L;
	
	private static final int PANEL_WIDTH = 500;
	private static final int PANEL_HEIGHT = 500;
	
	private Model model;
	
	public void setModel(Model model) {
		this.model = model;
		setFocusable(false);
	}
	
	public Canvas() {
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(PANEL_WIDTH, PANEL_HEIGHT);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		model.drawComponents(g);
	}
	
	public void refresh() {
		repaint();
	}
}
