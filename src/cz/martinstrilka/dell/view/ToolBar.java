package cz.martinstrilka.dell.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import cz.martinstrilka.dell.view.listeners.ToolBarActionListener;

public class ToolBar extends JToolBar {

	private static final long serialVersionUID = 3599029293512479135L;
	
	public static final String RECTANGLE_CMD = "rectangle";
	public static final String RECTANGLE = "Rct";
	public static final String RECTANGLE_TOOLTIP = "Click or drag mouse to draw rectangle.";
	public static final String TRIANGLE_CMD = "triangle";
	public static final String TRIANGLE = "Tri";
	public static final String TRIANGLE_TOOLTIP = "Click or drag mouse to draw triangle.";
	public static final String CIRCLE_CMD = "circle";
	public static final String CIRCLE = "Crc";
	public static final String CIRCLE_TOOLTIP = "Click or drag mouse to draw circle.";
	public static final String SELECT_CMD = "select";
	public static final String SELECT = "Select";
	public static final String SELECT_TOOLTIP = "Click on shape to select.";
	public static final String GROUP_CMD = "group";
	public static final String GROUP = "Group Selected";
	public static final String GROUP_TOOLTIP = "Makes group of selected shapes.";
	
	private JToggleButton btnRectangle;
	private JToggleButton btnCircle;
	private JToggleButton btnTriangle;
	private JToggleButton btnSelect;
	private JButton btnGroup;
	private ButtonGroup buttonGroup;
	
	public ToolBar() {
		super("Paint Toolbar");
		setOrientation(SwingConstants.VERTICAL);
		setFloatable(false);
		setFocusable(false);
	}
	
	public void initComponent() {
		this.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		
		buttonGroup = new ButtonGroup();
		
		//Rectangle
		constraints.gridx = 0;
		constraints.gridy = 0;
		btnRectangle = createButton(RECTANGLE, RECTANGLE_CMD, RECTANGLE_TOOLTIP, constraints);
		btnRectangle.setSelected(true);
		
		//Circle
		constraints.gridx = 1;
		constraints.gridy = 0;
		btnCircle = createButton(CIRCLE, CIRCLE_CMD, CIRCLE_TOOLTIP, constraints);
		
		//Triangle
		constraints.gridx = 2;
		constraints.gridy = 0;
		btnTriangle = createButton(TRIANGLE, TRIANGLE_CMD, TRIANGLE_TOOLTIP, constraints);
		
		//Select
		constraints.gridwidth = 3;
		constraints.gridx = 0;
		constraints.gridy = 1;
		btnSelect = createButton(SELECT, SELECT_CMD, SELECT_TOOLTIP, constraints);
		
		//Group Selected
		constraints.gridwidth = 3;
		constraints.gridx = 0;
		constraints.gridy = 2;
		btnGroup = new JButton(GROUP);
		btnGroup.setActionCommand(GROUP_CMD);
		btnGroup.setFocusable(false);
		btnGroup.setToolTipText(GROUP_TOOLTIP);
		add(btnGroup, constraints);
		
		//dummy
		constraints.anchor = GridBagConstraints.SOUTH;
		constraints.fill = GridBagConstraints.VERTICAL;
		constraints.gridwidth = 1;
		constraints.gridx = 0;
		constraints.gridy = 3;
		constraints.weighty = 1;
		add(new JLabel(), constraints);
	}
	
	public void addActionListener(ToolBarActionListener listener) {
		btnCircle.addActionListener(listener);
		btnRectangle.addActionListener(listener);
		btnTriangle.addActionListener(listener);
		btnSelect.addActionListener(listener);
		btnGroup.addActionListener(listener);
	}
	
	private JToggleButton createButton(String name, String action, String tooltip, GridBagConstraints constraints) {
		JToggleButton btn = new JToggleButton(name);
		btn.setActionCommand(action);
		btn.setFocusable(false);
		btn.setToolTipText(tooltip);
		buttonGroup.add(btn);
		add(btn, constraints);
		return btn;
	}
}
