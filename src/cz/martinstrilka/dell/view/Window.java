package cz.martinstrilka.dell.view;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import cz.martinstrilka.dell.controller.Controller;
import cz.martinstrilka.dell.shapes.Model;
import cz.martinstrilka.dell.view.listeners.CanvasMouseListener;
import cz.martinstrilka.dell.view.listeners.CanvasMouseMotionListener;
import cz.martinstrilka.dell.view.listeners.ToolBarActionListener;
import cz.martinstrilka.dell.view.listeners.WindowKeyListener;

public class Window extends JFrame {
	
	private static final long serialVersionUID = 8551128848702014447L;
	private static final String NAME = "Paint application";
	private ToolBar toolbar;
	private Canvas canvas;
	
	public Window() {
		super(NAME);
		setFocusable(true);
	}
	
	public void initUI() {
		setLayout(new BorderLayout());
		initToolBar();
		initCanvas();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
	}
	
	public void addListenersToComponents(Controller controller) {
		canvas.addMouseListener(new CanvasMouseListener(controller));
		canvas.addMouseMotionListener(new CanvasMouseMotionListener(controller));
		toolbar.addActionListener(new ToolBarActionListener(controller));
		addKeyListener(new WindowKeyListener(controller));
	}

	public void setModelToCanvas(Model model) {
		canvas.setModel(model);
	}

	public void refresh() {
		canvas.refresh();
	}
	
	private void initCanvas() {
        canvas = new Canvas();
        add(canvas, BorderLayout.CENTER);
	}
	
	private void initToolBar() {
		toolbar = new ToolBar();
		toolbar.initComponent();
		add(toolbar, BorderLayout.WEST);
	}

}
