package cz.martinstrilka.dell.utils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class Logger {

	private static final String FILE_NAME = "log.log";
	private static final Path PATH = Paths.get(FILE_NAME);
	private static final Charset charset = Charset.forName("US-ASCII");
	
	public static void log(String string) {
		System.out.println(string);
		
		try (BufferedWriter writer = Files.newBufferedWriter(PATH, charset, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
			writer.write(string);
			writer.write("\n");
		} catch(IOException ex) {
			ex.printStackTrace();
		}
	}
}
