package cz.martinstrilka.dell;

import javax.swing.SwingUtilities;

import cz.martinstrilka.dell.controller.Controller;

public class Main {

	// comment
	public static void main(String[] args) {
		final Controller controller = new Controller();
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				controller.createAndShowUI();
			}
		});

	}
}
