package cz.martinstrilka.dell.shapes.shapesSwing;

import cz.martinstrilka.dell.shapes.AbstractGraphicFactory;

public class GraphicFactorySwing extends AbstractGraphicFactory {

	private static final int DEFAULT_WIDTH = 30;
	private static final int DEFAULT_HEIGHT = 30;
	private static final int DEFAULT_DIAMETER = 30;
	private static final int DEFAULT_X = 0;
	private static final int DEFAULT_Y = 0;
	
	@Override
	public RectangleSwing createRectangle() {
		return new RectangleSwing(DEFAULT_X, DEFAULT_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}

	@Override
	public RectangleSwing createRectangle(int x, int y) {
		return new RectangleSwing(x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}

	@Override
	public RectangleSwing createRectangle(int x, int y, int width, int height) {
		return new RectangleSwing(x, y, width, height);
	}
	
	@Override
	public TriangleSwing createTriangle() {
		return new TriangleSwing(DEFAULT_X, DEFAULT_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}
	
	@Override
	public TriangleSwing createTriangle(int x, int y) {
		return new TriangleSwing(x, y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}

	@Override
	public TriangleSwing createTriangle(int x, int y, int width, int height) {
		return new TriangleSwing(x, y, width, height);
	}

	@Override
	public CircleSwing createCircle() {
		return new CircleSwing(DEFAULT_X, DEFAULT_Y, DEFAULT_DIAMETER);
	}
	
	@Override
	public CircleSwing createCircle(int x, int y) {
		return new CircleSwing(x, y, DEFAULT_DIAMETER);
	}

	@Override
	public CircleSwing createCircle(int x, int y, int diameter) {
		return new CircleSwing(x, y, diameter);
	}
}
