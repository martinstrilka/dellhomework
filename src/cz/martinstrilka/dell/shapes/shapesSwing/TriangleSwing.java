package cz.martinstrilka.dell.shapes.shapesSwing;

import java.awt.Graphics;

import cz.martinstrilka.dell.shapes.Triangle;

public class TriangleSwing extends Triangle {

	public TriangleSwing(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

	@Override
	public void draw(Object pen) {
		Graphics g = (Graphics) pen;
		if(selected || highlighted)
			g.setColor(getSelectedColor(COLOR));
		else
			g.setColor(COLOR);
		g.fillPolygon(new int[]{point1[0], point2[0], point3[0]},
				new int[]{point1[1], point2[1], point3[1]}, 3);
		if(selected || highlighted)
			g.setColor(getSelectedColor(BORDER_COLOR));
		else
			g.setColor(BORDER_COLOR);
		g.drawPolygon(new int[]{point1[0], point2[0], point3[0]},
				new int[]{point1[1], point2[1], point3[1]}, 3);
	}

}
