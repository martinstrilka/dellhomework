package cz.martinstrilka.dell.shapes.shapesSwing;

import java.awt.Graphics;

import cz.martinstrilka.dell.shapes.Rectangle;

public class RectangleSwing extends Rectangle {
	
	public RectangleSwing(int x, int y, int width, int height) {
		super(x, y, width, height);
	}
	
	@Override
	public void draw(Object pen) {
		Graphics g = (Graphics) pen;
		if(selected || highlighted)
			g.setColor(getSelectedColor(COLOR));
		else
			g.setColor(COLOR);
		g.fillRect(x, y, width, height);
		if(selected || highlighted)
			g.setColor(getSelectedColor(BORDER_COLOR));
		else
			g.setColor(BORDER_COLOR);
		g.drawRect(x, y, width, height);
	}

}
