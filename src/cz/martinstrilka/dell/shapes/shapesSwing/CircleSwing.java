package cz.martinstrilka.dell.shapes.shapesSwing;

import java.awt.Graphics;

import cz.martinstrilka.dell.shapes.Circle;

public class CircleSwing extends Circle {
	
	public CircleSwing(int x, int y, int diameter) {
		super(x, y, diameter);
	}

	@Override
	public void draw(Object pen) {
		Graphics g = (Graphics) pen;
		if(selected || highlighted)
			g.setColor(getSelectedColor(COLOR));
		else
			g.setColor(COLOR);
		g.fillOval(x, y, diameter, diameter);
		if(selected || highlighted)
			g.setColor(getSelectedColor(BORDER_COLOR));
		else
			g.setColor(BORDER_COLOR);
		g.drawOval(x, y, diameter, diameter);
	}

}
