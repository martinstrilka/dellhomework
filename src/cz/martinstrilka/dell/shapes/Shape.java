package cz.martinstrilka.dell.shapes;

import java.awt.Color;

public abstract class Shape extends Graphic {
	
	protected static final Color COLOR = Color.ORANGE;
	protected static final Color BORDER_COLOR = Color.BLACK;
	
	protected int x;
	protected int y;
	
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public void setX(int x) {
		this.x = x;
	}
	public void setY(int y) {
		this.y = y;
	}

	public Shape(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	protected static Color getSelectedColor(Color color) {
		double alpha = 0.5;
		double alpha_orig = 1.0 - alpha;
		return new Color(
				(int)(color.getRed() * alpha_orig + Color.white.getRed() * alpha),
				(int)(color.getGreen() * alpha_orig + Color.white.getGreen() * alpha),
				(int)(color.getBlue() * alpha_orig + Color.white.getBlue() * alpha));
	}
	
	@Override
	public String toString(int indent) {
		StringBuilder output = new StringBuilder();
		for (int i = 0; i < indent; i++) { output.append("\t"); }
		output.append("This shape is ");
		output.append(getName());
		output.append(" and is on: ");
		output.append(x);
		output.append(", ");
		output.append(y);
		return output.toString();
	}
}
