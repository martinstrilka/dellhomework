package cz.martinstrilka.dell.shapes;

import java.util.List;

public abstract class AbstractGraphicFactory {

	public abstract Rectangle createRectangle();
	public abstract Rectangle createRectangle(int x, int y);
	public abstract Rectangle createRectangle(int x, int y, int width, int height);
	
	public abstract Triangle createTriangle();
	public abstract Triangle createTriangle(int x, int y);
	public abstract Triangle createTriangle(int x, int y, int widht, int height);
	
	public abstract Circle createCircle();
	public abstract Circle createCircle(int x, int y);
	public abstract Circle createCircle(int x, int y, int i);
	
	public Group createGroup(List<Graphic> list) {
		Group group = new Group();
		for (Graphic graphic : list) {
			group.add(graphic);	
			graphic.setParrent(group);
		}
		return group;
	}
}
