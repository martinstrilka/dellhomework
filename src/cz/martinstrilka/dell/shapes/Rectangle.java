package cz.martinstrilka.dell.shapes;

public abstract class Rectangle extends Shape {

	protected int height;
	protected int width;
	
	private static final String name = "RECTANGLE"; 
	
	@Override
	public String getName() {
		return name;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
	
	public Rectangle(int x, int y, int width, int height) {
		super(x, y);
		this.width = width;
		this.height = height;
	}
	
	@Override
	public boolean isPointInsideShape(int pointX, int pointY) {
		if (pointX >= x && pointX <= x+width && pointY >= y && pointY <= y+height)
			return true;
		return false;
	}
}
