package cz.martinstrilka.dell.shapes;

public abstract class Graphic {

	protected boolean selected;
	protected boolean highlighted;
	protected Graphic parrent;
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setSelected(boolean value) {
		this.selected = value;
	}
	
	public boolean isHighlighted() {
		return highlighted;
	}

	public void setHighlighted(boolean value) {
		this.highlighted = value;
	}

	public Graphic getParrent() {
		return parrent;
	}
	
	public void setParrent(Graphic parrent) {
		this.parrent = parrent;
	}
	
	public Graphic getRoot() {
		if (parrent == null)
			return null;
		if (parrent != null && parrent.getParrent() == null)
			return parrent;
		if (parrent != null && parrent.getParrent() != null)
			return parrent.getRoot();
		return null;
	}
	
	public abstract String getName();
	public abstract String toString(int indent);
	//TODO zkusit udelat interface Pen
	public abstract void draw(Object pen);
	public abstract boolean isPointInsideShape(int pointX, int pointY);
}
