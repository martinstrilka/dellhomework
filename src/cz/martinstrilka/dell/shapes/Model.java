package cz.martinstrilka.dell.shapes;

import java.util.ArrayList;
import java.util.List;

import cz.martinstrilka.dell.utils.Logger;

public class Model {

	private static final String DELETE_MSG = "Deleted graphic. ";
	private static final String SELECT_MSG = "Selected graphic. ";
	private static final String GROUP_MSG = "Grouped graphic. ";
	
	private AbstractGraphicFactory graphicFactory;
	private List<Graphic> selected;
	private List<Shape> orderOfDrawing;
	private List<Group> groups;
	private Shape nowDrawing;

	public Shape getNowDrawing() {
		return nowDrawing;
	}

	public void setNowDrawing(Shape nowDrawing) {
		this.nowDrawing = nowDrawing;
	}
	
	public List<Graphic> getSelected() {
		return selected;
	}
	
	public void setGraphicFactory(AbstractGraphicFactory graphicFactory) {
		this.graphicFactory = graphicFactory;
	}

	public Model() {
		selected = new ArrayList<Graphic>();
		orderOfDrawing = new ArrayList<Shape>();
		groups = new ArrayList<Group>();
	}
	
	public void drawComponents(Object pen) {
		for (Graphic item : orderOfDrawing) {
			item.draw(pen);
		}
		if (nowDrawing != null) {
			nowDrawing.draw(pen);
		}
	}
	
	public void addShape(Shape shape) {
		orderOfDrawing.add(shape);
	}

	public void selectOrDeselectGraphic(int x, int y) {
		for (int i = orderOfDrawing.size() - 1; i >= 0 ; i--) {
			Graphic graphic = orderOfDrawing.get(i);
			if (!graphic.isPointInsideShape(x, y)) {
				continue;
			}
			if (graphic.getParrent() != null) {
				graphic = graphic.getRoot();
			}
			if (graphic.isSelected()) {
				graphic.setSelected(false);
				selected.remove(graphic);
				break;
			}
			else {
				graphic.setSelected(true);
				selected.add(graphic);
				Logger.log(SELECT_MSG + graphic.toString(0));
				break;
			}
		}
	}

	public void groupSelected() {
		if(selected.isEmpty()) {
			return;
		}
		Group group = graphicFactory.createGroup(selected);
		groups.add(group);
		selected.clear();
		selected.add(group);
		group.setSelected(true);
		Logger.log(GROUP_MSG + group.toString(0));
	}

	public void deleteSelectedGraphic() {
		if(selected.isEmpty()) {
			return;
		}
		Logger.log(DELETE_MSG);
		for (Graphic graphic : selected) {
			if (graphic instanceof Group) {
				Group group = (Group) graphic;
				orderOfDrawing.removeAll(group.getAllChildsAsList());
				groups.remove(group);
			}
			else {
				orderOfDrawing.remove(graphic);
			}
			Logger.log(graphic.toString(1));
		}
		selected.clear();
	}

	public void highlight(int x, int y) {
		for (Graphic graphic : orderOfDrawing) {
			graphic.setHighlighted(false);
		}
		for (Group group : groups) {
			group.setHighlighted(false);
		}
		for (int i = orderOfDrawing.size() - 1; i >= 0 ; i--) {
			Graphic graphic = orderOfDrawing.get(i);
			if (!graphic.isPointInsideShape(x, y)) {
				continue;
			}
			if (graphic.getParrent() != null) {
				graphic = graphic.getRoot();
			}
			graphic.setHighlighted(true);
			break;
		}
	}
	
}
