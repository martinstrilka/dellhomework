package cz.martinstrilka.dell.shapes;

import java.util.ArrayList;
import java.util.List;

public class Group extends Graphic {
	
	private List<Graphic> childs;
	private static String name = "GROUP";
	
	public Group() {
		childs = new ArrayList<Graphic>();
	}
	
	public void add(Graphic graphic) {
		childs.add(graphic);
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public void setSelected(boolean value) {
		super.setSelected(value);
		for (Graphic child : childs) {
			child.setSelected(value);
		}
	}
	
	@Override
	public void setHighlighted(boolean value) {
		super.setHighlighted(value);
		for (Graphic child : childs) {
			child.setHighlighted(value);
		}
	}

	@Override
	public void draw(Object pen) {
		for (Graphic item : childs) {
			item.draw(pen);
		}
	}
	
	@Override
	public String toString(int indent) {
		StringBuilder output = new StringBuilder();
		for (int i = 0; i < indent; i++) { output.append("\t"); }
		indent++;
		output.append("This shape is ");
		output.append(name);
		output.append(" and contains :");
		for (Graphic child : childs) {
			output.append("\n");
			output.append(child.toString(indent));
		}
		return output.toString();
	}

	@Override
	public boolean isPointInsideShape(int pointX, int pointY) {
		for (Graphic child : childs) {
			if (child.isPointInsideShape(pointX, pointY))
				return true;
		}
		return false;
	}

	public List<Graphic> getAllChildsAsList() {
		List<Graphic> list = new ArrayList<>();
		for (Graphic graphic : childs) {
			if (graphic instanceof Group) {
				list.addAll(((Group) graphic).getAllChildsAsList());
			} else {
				list.add(graphic);
			}
		}
		return list;
	}
}
