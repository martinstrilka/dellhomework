package cz.martinstrilka.dell.shapes;


public abstract class Circle extends Shape {

	private static final String name = "CIRCLE";
	
	protected int diameter;
	
	public int getDiameter() {
		return diameter;
	}

	public void setDiameter(int diameter) {
		this.diameter = diameter;
	}

	@Override
	public String getName() {
		return name;
	}
	
	public Circle(int x, int y, int diameter) {
		super(x, y);
		this.diameter = diameter;
	}
	
	@Override
	public boolean isPointInsideShape(int pointX, int pointY) {
		if (pointX < x || pointX > x + diameter || pointY < y || pointY > y + diameter)
			return false;
		int[] center = new int[] {x + diameter / 2, y + diameter / 2};
		double distance = Math.sqrt(Math.pow(Math.abs(center[0] - pointX), 2) + Math.pow(Math.abs(center[1] - pointY), 2));
		if (distance <= diameter / 2D)
			return true;
		return false;
	}
}
