package cz.martinstrilka.dell.shapes;

public abstract class Triangle extends Shape {
	
	private static final String name = "TRIANGLE";
	
	protected int[] point1;
	protected int[] point2;
	protected int[] point3;
	
	public Triangle(int x, int y, int width, int height) {
		super(x, y);
		this.point1 = new int[2];
		this.point2 = new int[2];
		this.point3 = new int[2];
		convertWidthHeightToPoints(width, height);
	}
	
	@Override
	public String getName() {
		return name;
	}

	public void setWidthAndHeight(int width, int height) {
		convertWidthHeightToPoints(width, height);
	}
	
	protected void convertWidthHeightToPoints(int width, int height) {
		this.point1[0] = x;
		this.point1[1] = y + height;
		this.point2[0] = x + width;
		this.point2[1] = y + height;
		this.point3[0] = x + width / 2;
		this.point3[1] = y;
	}
	
	@Override
	public boolean isPointInsideShape(int pointX, int pointY) {
		if (pointX < x || pointX > x + (point2[0] - point1[0]) || pointY < y || pointY > y + (point1[1] - point3[1]))
			return false;
		int[] point = new int[] {pointX, pointY};
		if (sameSide(point, point1, point2, point3) &&
			sameSide(point, point2, point1, point3) &&
			sameSide(point, point3, point1, point2))
			return true;
		return false;
	}
	
	private boolean sameSide(int[] p1, int[] p2, int[] a, int[] b) {
		int[] ab = new int[] {b[0] - a[0], b[1] - a[1]};
		int[] ap1 = new int[] {p1[0] - a[0], p1[1] - a[1]};
		int[] ap2 = new int[] {p2[0] - a[0], p2[1] - a[1]};
		long cp1 = crossProduct(ab, ap1);
		long cp2 = crossProduct(ab, ap2);
		if (cp1 * cp2 >= 0)
			return true;
		return false;
	}
	
	private int crossProduct(int[] vect1, int[] vect2) {
		return vect1[0] * vect2[1] - vect2[0] * vect1[1];
	}
}
