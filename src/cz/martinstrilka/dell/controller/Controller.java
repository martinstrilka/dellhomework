package cz.martinstrilka.dell.controller;

import cz.martinstrilka.dell.shapes.AbstractGraphicFactory;
import cz.martinstrilka.dell.shapes.Model;
import cz.martinstrilka.dell.shapes.shapesSwing.GraphicFactorySwing;
import cz.martinstrilka.dell.view.Window;

public class Controller {

	private Model model;
	private AbstractGraphicFactory graphicFactory;
	private Window window;
	
	private State currentState;
	private RectangleState rectangleState;
	private TriangleState triangleState;
	private CircleState circleState;
	private SelectState selectState;
	
	public Controller() {
		window = new Window();
		model = new Model();
		graphicFactory = new GraphicFactorySwing();
		model.setGraphicFactory(graphicFactory);
		
		rectangleState = new RectangleState(model, graphicFactory);
		triangleState = new TriangleState(model, graphicFactory);
		circleState = new CircleState(model, graphicFactory);
		selectState = new SelectState(model, graphicFactory);
		currentState = rectangleState;
	}
	
	public void createAndShowUI() {
		window.initUI();
		window.addListenersToComponents(this);
		window.setModelToCanvas(model);
		window.setVisible(true);
	}
	
	public void mouseClicked(int x, int y) {
		currentState.mouseClicked(x, y);
		window.refresh();
	}
	
	public void mousePressed(int x, int y) {
		currentState.mousePressed(x, y);
		window.refresh();
	}
	
	public void mouseReleased(int x, int y) {
		currentState.mouseReleased(x, y);
		window.refresh();
	}

	public void mouseDragged(int x, int y) {
		currentState.mouseDragged(x, y);
		window.refresh();
	}

	public void switchStateRectangle() {
		currentState = rectangleState;
	}
	
	public void switchStateTriangle() {
		currentState = triangleState;
	}

	public void switchStateCircle() {
		currentState = circleState;
	}
	
	public void switchStateSelect() {
		currentState = selectState;
	}

	public void groupSelected() {
		model.groupSelected();
	}

	public void deleteSelected() {
		model.deleteSelectedGraphic();
		window.refresh();
	}

	public void mouseMoved(int x, int y) {
		model.highlight(x, y);
		window.refresh();
	}
}
