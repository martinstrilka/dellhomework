package cz.martinstrilka.dell.controller;

import cz.martinstrilka.dell.shapes.AbstractGraphicFactory;
import cz.martinstrilka.dell.shapes.Model;
import cz.martinstrilka.dell.shapes.Rectangle;
import cz.martinstrilka.dell.shapes.Shape;

public class RectangleState extends State {

	public RectangleState(Model model, AbstractGraphicFactory graphicFactory) {
		super(model, graphicFactory);
	}
	
	@Override
	public void mouseClicked(int x, int y) {
		Rectangle rectangle = graphicFactory.createRectangle(x, y);
		model.addShape(rectangle);
	}

	@Override
	public void mousePressed(int x, int y) {
		this.isMousePressed = true;
		Shape rectangle = graphicFactory.createRectangle(x, y, 0, 0);
		model.setNowDrawing(rectangle);
		beginDrawingPoint[0] = x;
		beginDrawingPoint[1] = y;
	}

	@Override
	public void mouseReleased(int x, int y) {
		if (!this.isMousePressed) {
			return;
		}
		model.addShape(model.getNowDrawing());
		model.setNowDrawing(null);
		this.isMousePressed = false;
	}

	@Override
	public void mouseDragged(int x, int y) {
		if (!this.isMousePressed) {
			return;
		}
		updateDrawingPoints(x, y);
		Rectangle rect = (Rectangle) model.getNowDrawing();
		int height = rightDown[1] - leftUp[1];
		int width = rightDown[0] - leftUp[0];
		rect.setX(leftUp[0]);
		rect.setY(leftUp[1]);
		rect.setHeight(height);
		rect.setWidth(width);
	}

}
