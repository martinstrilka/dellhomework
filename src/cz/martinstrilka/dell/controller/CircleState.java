package cz.martinstrilka.dell.controller;

import cz.martinstrilka.dell.shapes.AbstractGraphicFactory;
import cz.martinstrilka.dell.shapes.Circle;
import cz.martinstrilka.dell.shapes.Model;
import cz.martinstrilka.dell.shapes.Shape;

public class CircleState extends State {

	public CircleState(Model model, AbstractGraphicFactory graphicFactory) {
		super(model, graphicFactory);
	}
	
	@Override
	public void mouseClicked(int x, int y) {
		model.addShape(graphicFactory.createCircle(x, y));
	}

	@Override
	public void mousePressed(int x, int y) {
		this.isMousePressed = true;
		Shape circle = graphicFactory.createCircle(x, y, 0);
		model.setNowDrawing(circle);
		beginDrawingPoint[0] = x;
		beginDrawingPoint[1] = y;
	}

	@Override
	public void mouseReleased(int x, int y) {
		if (!this.isMousePressed) {
			return;
		}
		model.addShape(model.getNowDrawing());
		model.setNowDrawing(null);
		this.isMousePressed = false;
	}

	@Override
	public void mouseDragged(int x, int y) {
		if (!this.isMousePressed) {
			return;
		}
		updateDrawingPoints(x, y);
		Circle circle = (Circle) model.getNowDrawing();
		int height = rightDown[1] - leftUp[1];
		int width = rightDown[0] - leftUp[0];
		int diameter = height > width ? height : width;
		circle.setDiameter(diameter);
		circle.setX(leftUp[0]);
		circle.setY(leftUp[1]);
	}

}
