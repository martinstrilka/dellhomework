package cz.martinstrilka.dell.controller;

import cz.martinstrilka.dell.shapes.AbstractGraphicFactory;
import cz.martinstrilka.dell.shapes.Model;
import cz.martinstrilka.dell.shapes.Shape;
import cz.martinstrilka.dell.shapes.Triangle;

public class TriangleState extends State {
	
	public TriangleState(Model model, AbstractGraphicFactory graphicFactory) {
		super(model, graphicFactory);
	}
	
	@Override
	public void mouseClicked(int x, int y) {
		Triangle triangle = graphicFactory.createTriangle(x, y);
		model.addShape(triangle);
	}

	@Override
	public void mousePressed(int x, int y) {
		this.isMousePressed = true;
		Shape triangle = graphicFactory.createTriangle(x, y, 0, 0);
		model.setNowDrawing(triangle);
		beginDrawingPoint[0] = x;
		beginDrawingPoint[1] = y;
	}

	@Override
	public void mouseReleased(int x, int y) {
		if (!this.isMousePressed) {
			return;
		}
		model.addShape(model.getNowDrawing());
		model.setNowDrawing(null);
		this.isMousePressed = false;
	}

	@Override
	public void mouseDragged(int x, int y) {
		if (!this.isMousePressed) {
			return;
		}
		updateDrawingPoints(x, y);
		Triangle triangle = (Triangle) model.getNowDrawing();
		int height = rightDown[1] - leftUp[1];
		int width = rightDown[0] - leftUp[0];
		triangle.setX(leftUp[0]);
		triangle.setY(leftUp[1]);
		triangle.setWidthAndHeight(width, height);
	}

}
