package cz.martinstrilka.dell.controller;

import cz.martinstrilka.dell.shapes.AbstractGraphicFactory;
import cz.martinstrilka.dell.shapes.Model;

public abstract class State {

	protected Model model;
	protected AbstractGraphicFactory graphicFactory;
	protected boolean isMousePressed;
	
	protected int[] beginDrawingPoint;
	protected int[] endDrawingPoint;
	protected int[] leftUp;
	protected int[] rightDown;

	public State(Model model, AbstractGraphicFactory graphicFactory) {
		this.model = model;
		this.graphicFactory = graphicFactory;
		beginDrawingPoint = new int[2];
		endDrawingPoint = new int[2];
		leftUp = new int[2];
		rightDown = new int[2];
	}
	
	public abstract void mouseClicked(int x, int y);
	public abstract void mousePressed(int x, int y);
	public abstract void mouseReleased(int x, int y);
	public abstract void mouseDragged(int x, int y);
	
	protected void updateDrawingPoints(int x, int y) {
		endDrawingPoint[0] = x;
		endDrawingPoint[1] = y;
		leftUp[0] = beginDrawingPoint[0] < endDrawingPoint[0] ? beginDrawingPoint[0] : endDrawingPoint[0];
		leftUp[1] =	beginDrawingPoint[1] < endDrawingPoint[1] ? beginDrawingPoint[1] : endDrawingPoint[1];
		rightDown[0] = beginDrawingPoint[0] > endDrawingPoint[0] ? beginDrawingPoint[0] : endDrawingPoint[0];
		rightDown[1] = beginDrawingPoint[1] > endDrawingPoint[1] ? beginDrawingPoint[1] : endDrawingPoint[1];
	}
	
}
