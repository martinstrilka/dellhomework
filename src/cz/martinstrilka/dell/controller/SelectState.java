package cz.martinstrilka.dell.controller;

import cz.martinstrilka.dell.shapes.AbstractGraphicFactory;
import cz.martinstrilka.dell.shapes.Model;

public class SelectState extends State {

	public SelectState(Model model, AbstractGraphicFactory graphicFactory) {
		super(model, graphicFactory);
	}

	@Override
	public void mouseClicked(int x, int y) {
		model.selectOrDeselectGraphic(x, y);
	}

	@Override
	public void mousePressed(int x, int y) {
			
	}

	@Override
	public void mouseReleased(int x, int y) {

	}

	@Override
	public void mouseDragged(int x, int y) {

	}

}
